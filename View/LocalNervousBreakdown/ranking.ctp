<!-- app/View/LocalNervousBreakdown/ranking.ctp -->
<table>
	<tr>
		<th>順位</th>
		<th>名前</th>
		<th>性別</th>
		<th>年代</th>
		<th>都道府県</th>
		<th>タイム</th>
	</tr>
<?php foreach ($ranking_data as $row): ?>
	<tr>
		<td><?php echo h($row['0']['rank']);?></td>
		<td><?php echo h($row['ranking']['name']);?></td>
		<td><?php echo h($row['ranking']['sex']);?></td>
		<td><?php echo h($row['ranking']['age']) .'代';?></td>
		<td><?php echo h($row['ranking']['live']);?></td>
		<td><?php
			if($row['ranking']['seconds'] < 10) {
				echo h($row['ranking']['minutes']) .':0' .h($row['ranking']['seconds']);
			} else {
				echo h($row['ranking']['minutes']) .':' .h($row['ranking']['seconds']);
			}
		?></td>
	</tr>
<?php endforeach;?>
</table>
