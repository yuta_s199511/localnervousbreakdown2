<?php
// app.Controller/LocalNervousBreakdownController.php

class LocalNervousBreakdownController extends AppController {
	
	public function index() {
		$this -> autoLayout = false;  // レイアウトをOFFにする
		$this -> ext = '.html';
		$this -> render('/LocalNervousBreakdown/main');
	}

	public function register() {
		$this -> autoLayout = false;  // レイアウトをOFFにする

		if($this -> request -> is('post')) {
			$data = array(
				'minutes' => $this -> request -> data['minutes'],
				'seconds' => $this -> request -> data['seconds']
			);
			$this -> set('data', $data);
			$this -> render('register');
		} else {
			$this -> redirect('/LocalNervousBreakdown');
		}
	}

	public function ranking() {
		$this -> autoLayout = false;  // レイアウトをOFFにする

		if($this -> request -> is('post')) {
			$data = array(
				'name' => $this -> request -> data['name'],
				'minutes' => $this -> request -> data['minutes'],
				'seconds' => $this -> request -> data['seconds'],
				'sex' => $this -> request -> data['sex'],
				'age' => $this -> request -> data['age'],
				'live' => $this -> request -> data['live']
			);
			$id = $this -> LocalNervousBreakdown -> save($data);
		}

		$sql = "SELECT (SELECT COUNT(*) FROM ranking as r2 WHERE ranking.minutes > r2.minutes OR ranking.minutes = r2.minutes AND ranking.seconds > r2.seconds) + 1 as rank, name, minutes, seconds, sex, age, live FROM ranking ORDER BY rank";
		$ranking_data = $this -> LocalNervousBreakdown -> query($sql);
		$this -> set('ranking_data', $ranking_data);
		$this -> render('ranking');
	}
}
?>
