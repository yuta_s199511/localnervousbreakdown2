    ;(function(w) {
      w.app = w.app || {};

      app.NervousBreakdown = function() {
        this.CARD_NUM = 20,         // カード枚数
        this.QUESTION_NUM = 47,     // 総問題数
        this.$readmePage = $(".readme-page"),
        this.$start = $(".start"),
        this.$single = $(".single"),
        this.$singleStage = $(".single-game-stage"),  // カードを表示する場所
        this.$singleResult = $(".single-result"),
        this.$watchMinute = $(".watchMinute"),        // 経過時間（分）
        this.$watchSecond = $(".watchSecond");        // 経過時間（秒）
        this.$hint = $(".hint");                      // ヒント
        this.$explanation = $(".explanation");        // 解説
        this.$correct = $(".correct"),
        this.$incorrect = $(".incorrect"),
        this.$resultTime = $(".result-time");         // 結果タイム
        this.$first = undefined,             // 1枚目のカード
        this.$firstNum = undefined,          // 1枚目のカードの数
//        this.$correctNum = 0,                // そろえたカード
        this.$correctNum = 9,                // そろえたカード
        this.$flipFlag = true,               // 連打防止
        this.$checkQuestion = new Array(46), // 画像チェック用
        this.$quiz = new Array(20),          // 問題用
        this.$watch = undefined,             // 経過時間保存用

        this._initialize();                  // 初期化処理
      };

      app.NervousBreakdown.prototype = {
        _initialize: function() {
          var _this = this;

          $(".readme-btn").on("click", this._startReadMe.bind(this));
          $(".single-btn").on("click", this._startGame.bind(this));
          $(".ranking-btn").on("click", this._ranking.bind(this));
          $(".reset-btn").on("click", this._resetGame.bind(this));

          this.$singleStage.on("click", "li", function() {
            _this._flipCard($(this));
          });
        },
        _startReadMe: function() {
          this._changeView(this.$start, this.$readmePage);
        },
        _startGame: function() {
          this._changeView(this.$start, this.$single);
          $(this.$watchMinute).text("00");
          $(this.$watchSecond).text("00");
          this._initCards();
          this.$watch = setInterval(this._timer.bind(this), 1000);
        },
        _changeView: function($fadeOut, $fadeIn) {
          $fadeOut.fadeOut(700, function() {
            $fadeIn.fadeIn(700);
          });
        },
        _initCards: function() {
          this.flipFlag = true;
          this.$singleStage.html("");

          this.$singleStage.append(this._getTemplate());
        },
        _getTemplate: function() {
          var num,
              cardIndex,
              cards = [],
              list = "";

          for (var i = 0; i < this.CARD_NUM / 2; i++) {
            num = Math.floor(Math.random() * this.QUESTION_NUM);
            if (this.$checkQuestion[num] != 1) {
              this.$quiz[i] = num;
              this.$quiz[10 + i] = num + 50;
              this.$checkQuestion[num] = 1;
            } else {
              i--;
            }
          }

          for (var i = 0; i < this.CARD_NUM; i++) {
            cardIndex = Math.floor(Math.random() * this.CARD_NUM);
            if (cards[cardIndex] == undefined) {
              cards[cardIndex] = this.$quiz[i];
            } else {
              i--;
            }
          }

          for (var j = 0; j < this.CARD_NUM; j++) {
            list += '<li id=' + cards[j] + '></li>';
          }

          return list;
        },
        _flipCard: function($this) {
          if (!this.flipFlag || $this.attr("class") == 'is-clear' || $this.attr("class") == 'is-flip') {
            return false;
          }

          var flipNum = $this.attr("id");

          $this.addClass("is-flip");
          $this.css('background-image', 'url(app/webroot/img/card/' + flipNum +'.png)');

          if (this.$firstNum == undefined) {
            this.$firstNum = parseInt(flipNum);
            this.$first = $this;
            $(this.$hint).text("");
            $(this.$explanation).text("");
          } else {
            this.flipFlag = false;
            this._judgeCards($this, parseInt(flipNum));
          }
        },
        _judgeCards: function($second, secondNum) {
          var _this = this;

          setTimeout(function() {
            $(_this.$first).removeClass("is-flip");
            $($second).removeClass("is-flip");
            if (_this.$firstNum + 50 == secondNum || _this.$firstNum == secondNum + 50) {
              _this.$correctNum++;
              $(_this.$explanation).load("app/webroot/files/explanation" + (_this.$firstNum % 50) + ".txt");
              _this.$correct.fadeIn(500);
              setTimeout(function() {
                _this.$correct.fadeOut(500);
                $(_this.$first).addClass("is-clear");
                $($second).addClass("is-clear");

                if (_this.$correctNum == _this.CARD_NUM / 2) {
                  clearInterval(_this.$watch);
                  $(_this.$resultTime).text($(_this.$watchMinute).text() + " : " + $(_this.$watchSecond).text());
                  setTimeout(function() {
                    _this.$singleResult.fadeIn(700);
                  }, 500);
                }
              }, 500);
            } else {
              $(_this.$hint).load("app/webroot/files/hint" + (_this.$firstNum % 50) + ".txt");
              _this.$incorrect.fadeIn(500);
              setTimeout(function() {
                _this.$incorrect.fadeOut(500);
              }, 500);
              setTimeout(function() {
                _this.$first.css('background-image', 'url(app/webroot/img/card/reverse.png)');
                $second.css('background-image', 'url(app/webroot/img/card/reverse.png)');
              }, 500);
            }
            setTimeout(function() {
              _this.flipFlag = true;
              _this.$first = undefined;
              _this.$firstNum = undefined;
            }, 800);
          }, 800);
        },
        _ranking: function() {
          console.log("hoge");
          $(':hidden[name="minutes"]').val(parseInt($(this.$watchMinute).text()));
          $(':hidden[name="seconds"]').val(parseInt($(this.$watchSecond).text()));
          $('#ranking_form').submit();
//          location.href = 'LocalNervousBreakdown/register';
        },
        _resetGame: function() {
          location.reload();
        },
        _timer: function() {
          var minute = parseInt($(this.$watchMinute).text());
          var second = parseInt($(this.$watchSecond).text()) + 1;
          if(second == 60) {
            second = 0;
            minute++;
          }
          if(second < 10) {
            second = '0' + second;
          }
          if(minute < 10) {
            minute = '0' + minute;
          }
          $(this.$watchMinute).text(minute);
          $(this.$watchSecond).text(second);
        }
      };
    })(window);
    $(function() {
      var pelmanism = new app.NervousBreakdown();
    });